package java;

import java.util.Objects;

import static kotlin.KotlinKt.randomString;


// Data class

class Event {

    private String id;
    private String type;
    private String description;

    Event(String id, String type, String description) {
        this.id = id;
        this.type = type;
        this.description = description;
    }

    Event(String id, String type) {
        this(id, type, "No text");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(id, event.id) &&
                Objects.equals(type, event.type) &&
                Objects.equals(description, event.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, type, description);
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", text='" + description + '\'' +
                '}';
    }
}


// Explicit cast

class ExplicitCast {

    String getId(Object event) {
        return (event instanceof Event) ? ((Event) event).getId() : randomString();
    }
}


// String concatenation
// See "toString()" in Event


// Switch

class Switch {
    String getMonthString(int month) {
        String monthString;
        switch (month) {
            case 1:
                monthString = "January";
                break;
            case 2:
                monthString = "February";
                break;
            case 3:
                monthString = "March";
                break;
            case 4:
                monthString = "April";
                break;
            case 5:
                monthString = "May";
                break;
            case 6:
                monthString = "June";
                break;
            case 7:
                monthString = "July";
                break;
            case 8:
                monthString = "August";
                break;
            case 9:
                monthString = "September";
                break;
            case 10:
                monthString = "October";
                break;
            case 11:
                monthString = "November";
                break;
            case 12:
                monthString = "December";
                break;
            default:
                monthString = "Month not found";
                break;
        }
        return monthString;
    }
}

// "Safety" features

class Notification {

    private String text;

    Notification(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Notification prettifyText(Prettify prettify) {
        text = prettify.prettify(text);
        return this;
    }
}

// Null check

class Client {
    String getText(Notification notification) {
        if (notification == null || notification.getText() == null) return "default notification";
        return notification.getText();
    }
}

// Inheritance

class EmailNotification extends Notification {
    EmailNotification(String text) {
        super(text);
    }
}

// Lambda

interface Prettify {
    String prettify(String text);
}

class NotificationClient {

    Notification prettify(Notification notification) {
        return notification.prettifyText( text -> "Pretty: " + text );
    }
}

// Static function

class Utility {

    static String getSomething() {
        return "something";
    }
}

// Default null

class Final {
    String tbc;
}
