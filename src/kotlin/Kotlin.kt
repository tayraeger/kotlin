package kotlin

import java.util.*

fun randomString() = UUID.randomUUID().toString()


// Data class

data class Event(
    val id: String,
    var type: String,
    val description: String = "No text"
)

// Declare variables and functions at file level
// Default & named arguments

val event: Event = Event(id = randomString(), type = randomString())


// Inferred type

val event2 = Event(id = randomString(), type = randomString())


// Smart cast

fun getId(event: Any): String {
    return if (event is Event) event.id else randomString()
}


// Expression and return type omission

fun getId2(event: Any) = if (event is Event) event.id else null


// String interpolation

fun toString1(event: Event) = "Event{'id': $event.id, 'type': ${event.type}, 'text': ${event.description}}"


// Scope functions

fun toString2(event: Event) = with(event, {
    "Event{'id': $this.id, 'type': $this.type, 'text': $this.description}"
})

// Last argument is lambda

fun toString3(event: Event) = with(event) {
    "Event{'id': $id, 'type': $type, 'text': $description}"
}

fun toString4(event: Event) = event.let { event ->
    "Event{'id': $event.id, 'type': $event.type, 'text': $event.description}"
}

// "it"

fun toString5(event: Event) = event.let {
    "Event{'id': $it.id, 'type': $it.type, 'text': $it.description}"
}

// Destructuring

fun toString6(): String {
    val (id, type, description) = event
    return "Event{'id': $id, 'type': $type, 'text': $description}"
}

fun toString7(event: Event) = event.let { (id, type, description) ->
    "Event{'id': $id, 'type': $type, 'text': $description}"
}

// Extension function

fun Event.toString8() = "Event{'id': $id, 'type': $type, 'text': $description}"

val eventString = event.toString8()


// When

fun getMonthString(month: Int) = when (month) {
    1 -> "January"
    2 -> "February"
    3 -> "March"
    4 -> "April"
    5 -> "May"
    6 -> "June"
    7 -> "July"
    8 -> "August"
    9 -> "September"
    10 -> "October"
    11 -> "November"
    12 -> "December"
    else -> "Month not found"
}

fun getSomething(n: Int) = when (n) {
    1, 3, 5 -> "Odd number"
    2, 4 -> "Even number"
    in 5..10 -> "Between 5 and 10"
    else -> "Something else"
}

fun getSomething2(n: Int) = when {
    n > 10 -> "Greater than 10"
    n < -10 -> "Lesser than -10"
    else -> "Everything else"
}


// "Safety" features

data class Notification(val text: String?)

// Null safe handing

fun getText1(notification: Notification?): String {
    return notification?.text ?: "default notification"
}

// NPE

fun getText2(notification: Notification?): String {
    return notification!!.text!!
}

// Inheritance

open class InheritableNotification(val text: String?)

class EmailNotification : InheritableNotification(randomString())


// Function type and lambdas

fun Notification.prettifyText(prettify: (String) -> String): Notification {
    return Notification(prettify(this.text!!))
}

val uglyNotification = Notification(randomString())

val prettyNotification = uglyNotification.prettifyText { "Pretty: $it" }


// Companion object

class Utility {
    companion object {
        fun getSomething() = "something"
    }
}

val something = Utility.getSomething()


// lateinit var

class Final {
     lateinit var tbc: String
}