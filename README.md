# Kotlin

## Topics

1. Conciseness
    * Data class
    * Default & named arguments
    * Smart cast
    * Return type omission
    * String interpolation & scope function
    * Deconstruction
    * Powerful `when`
2. Safety features
    * Null safety
    * Immutability
    * Final class by default (?)
3. Others
    * Extension functions
    * First-class function
    * No checked exception
    * DSL?
    * Extension helper functions
    * Immutable collections
    * Nested function
    * lateinit var
    * "it"
    * Last variable is function
Cons
1.  Harder to implement static members